# barrier-subtree



## Installation  
The following should work on all platforms:
```
python3 -m build
pip install .
```
Dependency: [https://gitlab.inria.fr/bmarchan/bisr-dpw](https://gitlab.inria.fr/bmarchan/bisr-dpw) which is installed similarly

## Usage
```
from rna_barrier_subtree_solver.subtree_solver import solve_subtree

s1 =".((...(((.)))))"
s2 ="((((.)((.).))))"

k, schedule = solve_subtree(s1, s2)
```
